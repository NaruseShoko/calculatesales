package jp.alhinc.naruse_shoko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {

/*
 * line：支店定義ファイルの中身(支店コードと支店名)
 * branchList：lineを格納する配列
 * branchLineList：「,」で区別した支店コードと支店名を入れるList
 * salesFile：売上ファイル名(拡張子rcdかつ8桁)
 * salesFileNameList：salesFileを格納する配列
 * sales：売上ファイルの中身(支店コードと売上金額)
 * salesArray：salesを格納する配列
 * salesArrayList：改行で区別した支店コードと売上を入れるList
 * sum：売上合計金額
 */

	public static void main(String[] args) {

//1．支店定義ファイル読み込み

		//エラー処理：コマンドライン引数が渡されていない場合はエラー文を出力
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String,String> branchNames = new HashMap<String,String>();
		HashMap<String,Long> branchSales = new HashMap<String,Long>();

		BufferedReader br = null;
		try {
			File branchFile = new File(args[0], "branch.list");

			//エラー処理：支店定義ファイルが存在しない場合
			if(!branchFile.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(branchFile);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] branchList;
				branchList = line.split(",");

				//エラー処理：支店定義ファイルのフォーマットが不正な場合
				if(!branchList[0].matches("^[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//エラー処理：支店定義ファイルのフォーマットが不正な場合
				if(branchList.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//支店コードと支店名をそれぞれペアで格納
				branchNames.put(branchList[0] , branchList[1]);
				//支店コードと売上の初期値をペアで格納
				branchSales.put(branchList[0], (long)0);
			}
		}

		//エラー処理：処理内容に記載のないエラーの場合
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		finally {
			if(br != null) {
				try {
					br.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}


//2-1．指定の売上ファイル検索

		ArrayList<String> salesFileNameList = new ArrayList<String>();

		File directory = new File(args[0]);
		File file[] = directory.listFiles();
		for(int i = 0; i < file.length; i++){
			String fileNames = file[i].getName();

			//拡張子rcdかつ8桁のファイル名を検索
			if(fileNames.matches("^[0-9]{8}.+rcd$")) {
				salesFileNameList.add(fileNames);
			}
		}

		for(int i = 0; i < salesFileNameList.size(); i++) {
			String fileNames1 = salesFileNameList.get(i).substring(0,8);
			String fileNames2 = salesFileNameList.get(i+1).substring(0,8);

			//エラー処理：売上ファイルが連番になっていない場合
			if(fileNames2 != fileNames1 +1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}


//2-2．売上ファイル読み込み、加算

		for(int i = 0; i < salesFileNameList.size(); i++) {
			try {
				File salesFile = new File(args[0], salesFileNameList.get(i));
				FileReader fr = new FileReader(salesFile);
				br = new BufferedReader(fr);
				String sales;
				ArrayList<String> salesArrayList = new ArrayList<String>();

				while((sales = br.readLine()) != null) {
					salesArrayList.add(sales);
				}

				//エラー処理：売上ファイルの中身が2行ではなかった場合
				if(salesArrayList.size() != 2) {
					System.out.println(salesFileNameList.get(i) + "のフォーマットが不正です");
					return;
				}

				//エラー処理：売上金額が数字でない場合
				if(!(salesArrayList.get(1).matches("^[0-9]+$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//エラー処理：支店コードが支店定義ファイルと一致しない場合
				if(!branchNames.containsKey(salesArrayList.get(0))){
					System.out.println(salesFileNameList.get(i) + "の支店コードが不正です");
					return;
				}
				long sum = branchSales.get(salesArrayList.get(0));
				sum += Integer.parseInt(salesArrayList.get(1));

				//エラー処理：集計した売上が10桁を超える場合
				if(String.valueOf(sum).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//branchSalesに集計結果を格納（キー：支店コード、値：集計売上）
				branchSales.put(salesArrayList.get(0) , sum);
			}

			//エラー処理：処理内容に記載のないエラーの場合
			catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			finally {
				if(br != null) {
					try {
						br.close();
					}
					catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		fileWrite(args,branchNames,branchSales);
	}

//3．集計結果出力

	public static void fileWrite(String[] args, HashMap<String, String> branchNames, HashMap<String, Long> branchSales) {
		BufferedWriter bw = null;
		try {
			File branchOut = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(branchOut);
			bw = new BufferedWriter(fw);
			for(HashMap.Entry<String, String> branch : branchNames.entrySet()){
				bw.write(branch.getKey() + "," + branch.getValue() + "," + branchSales.get(branch.getKey()));
				String lineFeed = System.lineSeparator();
				bw.write(lineFeed);
			}
		}

		//エラー処理：処理内容に記載のないエラーの場合
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		finally {
			if(bw != null) {
				try{
					bw.close(); //処理終了
				}
				catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}

